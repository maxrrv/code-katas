import { useState, useEffect } from 'react'
const HOME='home'
const PRODUCT_DETAIL = 'product_detail'

function App() {
  const [products, setProducts] = useState([])
  const [page, setPage] = useState(HOME)

  useEffect(() => {
    fetch('https://api.predic8.de/shop/products/')
      .then(data => data.json())
      .then(data => data.products)
      .then(data => setProducts(data))
  }, [])

  const Home = () => (
    products.map(product => <div>{product.name}</div>)
  )

  const ProductDetail = () => (
    <div />
  )

  return (
    <div className="App">
      <nav style={{display: 'flex'}}>
        <div style={{width: 100, textAlign: 'center'}}>
        <img style={{height: 50}} src='https://www.svgrepo.com/show/395700/picture-gallery-photo-image.svg'/>
        </div>
        <a style={{lineHeight: '50px', fontSize: '50px'}} href='./'>Home</a>
      </nav>
      { page === HOME && <Home /> }
      { page === PRODUCT_DETAIL && <ProductDetail /> }
    </div>
  );
}

export default App;
