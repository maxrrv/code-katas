# React katas
Task

* Display a header with a logo and a home page button
* Fetch an array with products from https://api.predic8.de/shop/products/
* Display all links with product names in a grid
* The home page button should go back to the product overview
* when clicking on a name, show a detailed view of product
* the image should be displayed on the left
* the name and the price should be displayed on the right
* the price should be displayed under the name
* the name should be a header
* add a fill text paragraph below the name / price block
